README.TXT
----------

Additional information relating to the build of design xCORE-200-MC-AUDIO_2V0_NONMFI.

Design Ref : xCORE-200-MC-AUDIO_2V0_NONMFI
Date    : 1st April 2015

PCB Design tool : Altium Designer 14


Description of files included in xCORE-200-MC-AUDIO_2V0_NONMFI.ZIP:

./README.TXT    This file.

./BOM/          This directory contains the bill of materials and component position files for assembly.

xCORE-200-MC-AUDIO_2V0.TXT          Component Position Data
xCORE-200-MC-AUDIO_2V0-BOM-NONMFI.XLS      Bill Of Materials

./Gerber/       This directory contains the following gerber files for the design.

xCORE-200-MC-AUDIO_2V0.GTL          Top Copper
xCORE-200-MC-AUDIO_2V0.GP1          Inner 1 Copper
xCORE-200-MC-AUDIO_2V0.GP2          Inner 2 Copper
xCORE-200-MC-AUDIO_2V0.GBL          Bottom Copper
xCORE-200-MC-AUDIO_2V0.GTO          Silkscreen Top
xCORE-200-MC-AUDIO_2V0.GBO          Silkscreen Bottom  
xCORE-200-MC-AUDIO_2V0.GTS          Solder Mask Top
xCORE-200-MC-AUDIO_2V0.GBS          Solder Mask Bottom
xCORE-200-MC-AUDIO_2V0.GTP          Solder Paste Top
xCORE-200-MC-AUDIO_2V0.GBP          Solder Paste Bottom
xCORE-200-MC-AUDIO_2V0.GM15         Additional component assembly information
xCORE-200-MC-AUDIO_2V0.GM17         Fabrication Instructions
xCORE-200-MC-AUDIO_2V0.GD1          Drill Diagram
xCORE-200-MC-AUDIO_2V0.GG1          Drill Guide

All gerbers are RS-274-X format, metric (0000.000), no zero suppression, absolute coordinates.

./Drill/        This directory contains the following drill data files for the design.

xCORE-200-MC-AUDIO_2V0-RoundHoles-NonPlated.TXT       	Non Plated Round Holes NC Drill File
xCORE-200-MC-AUDIO_2V0-RoundHoles-Plated.TXT      	Plated Round Holes NC Drill File
xCORE-200-MC-AUDIO_2V0-SlotHoles-Plated.TXT		Plated Slot Holes NC Drill File
xCORE-200-MC-AUDIO_2V0.DRR                		NC Drill Report (Sizes & Quantities)

NC Drill files are Excellon Format, metric (0000.000), no zero suppression, absolute coordinates.

./Netlist/      This directory contains the netlist file for equivalence checking and bare board test.

xCORE-200-MC-AUDIO_2V0.IPC          IPC-D-356 Format Netlist

./ODB++/        This directory contains an ODB++ file for the design.

xCORE-200-MC-AUDIO_2V0.ZIP          The design in ODB++ Format.